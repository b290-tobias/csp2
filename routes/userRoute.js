const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const userController = require("../controllers/userController.js");


// router for user registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// router for user login

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});



// Router for Non-admin User checkout (Create Order)


router.post("/checkout", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userData = auth.decode(req.headers.authorization);
	const userId = userData.id

	if (isAdmin) {
		console.log({ auth : `Unauthorized User`});
		res.send(false);
	} else {
		userController.order(userId, req.body).then(resultFromController => res.send(resultFromController));
	};
});



// Router for retrieve user details

router.get("/profile", auth.verify, (req, res) => {


	const userData = auth.decode(req.headers.authorization);
	const userId = userData.id


		userController.getprofile({ userId : userId }).then(resultFromController => res.send(resultFromController));
});




// Router for set user as Admin (admin only)

router.patch("/admin", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if (isAdmin) {
		userController.setAdmin(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(false);
	}

});



// Router for retrieve authenticated user's order

router.get("/myorders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const userId = userData.id

	userController.getMyOrders({ userId : userId }).then(resultFromController => res.send(resultFromController));
});




// Router for retrieve all orders (Admin only)

router.get("/allorders", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if (isAdmin) {
		userController.getAllOrders({}).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(false);
	}
});



/*// Router for add to Cart

router.post("/addtocart", auth.verify, (req, res) => {

	userController.addToCart(req.body).then(resultFromController => res.send(resultFromController));
})



*/








module.exports = router;