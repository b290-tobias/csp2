const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const productController = require("../controllers/productController.js");


// Router for Product Creation

router.post("/create", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`})
		res.send(`Unauthorized User`)
	}
});


// Router for retrieve all products

router.get("/all", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.getAllProducts(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(`Unauthorized User`);
	};

});



// Router for retrieve active products

router.get("/active", (req, res) => {

	productController.getActiveProducts(req.body).then(resultFromController => res.send(resultFromController));
});




// Router for Retrieve single product

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});



// Router for update Product information (admin only)

router.patch("/:productId/update", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(`Unauthorized User`);
	};
});



// Router for archive product (admin only)

router.patch("/:productId/archive", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if (isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(`Unauthorized User`)
	};
});




// Router for activate product (admin only)


router.patch("/:productId/activate", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if (isAdmin) {
		productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : `Unauthorized User`});
		res.send(`Unauthorized User`)
	};
});






module.exports = router;






