// Application
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Access Routes
const app = express();
const userRoute = require("./routes/userRoute.js");
const productRoute = require("./routes/productRoute.js")


// MongoDB
mongoose.connect("mongodb+srv://admin:admin123@zuitt.ziq0mma.mongodb.net/e-commerce?retryWrites=true&w=majority", 
		{
			useNewUrlParser	: true,
			useUnifiedTopology	: true
		}
);

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));


// Routes
app.use("/users", userRoute);
app.use("/products", productRoute);





// Module exports
if(require.main === module){
	app.listen(process.env.PORT || 5000, () => {
		console.log(`API is now online on port ${process.env.PORT || 5000}`)
	});
}

module.exports = { app, mongoose };