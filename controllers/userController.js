const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");


// User registration
module.exports.registerUser = reqBody => {

	return User.findOne({ email : reqBody.email })
		.then(result => {

			// Checks if email is registered
			// if yes - proceed with registration
			// if not - prompt email is already registered
			if (result == null ) {
				let newUser = new User({
					firstName : reqBody.firstName,
					lastName : reqBody.lastName,
					email : reqBody.email,
					password : bcrypt.hashSync(reqBody.password, 10)	
				});


				return newUser.save()
					.then(user => {
						// return `${user.firstName} is now registered`;
						return true;
					})
						.catch(err => {
							console.log(err);
							// return `Registration failed`;
							return false;
						});
			} else {

				// return `Email is already registered. Please enter a different email address`;
				return false;
			}
		}).catch(err => {
			console.log(err);
			console.log(`Registration error`);
			// return `Registration error`;
			return false;
		});
};





// User authentication / User login


module.exports.loginUser = reqBody => {

	return User.findOne({ email: reqBody.email})
		.then(result => {

			if (result == null) {
				console.log(`User is not registered`);
				return false;
			} else {

				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if (isPasswordCorrect) {
					return { access : auth.createAccessToken(result)};
				} else {
					console.log(`Password did not match`);
					return false;
				}
			}
		}).catch(err => {
			console.log(err);
			return false;
		});
};




// Non-admin User checkout (Create Order)

module.exports.order = async (userId, reqBody) => {

	let isUserUpdated = await User.findById(userId)
		.then(user => {

			//update the ordered products under user
			user.orderedProducts.push({
				products : {
					productId : reqBody.productId,
					productName : reqBody.productName,
					quantity : reqBody.quantity
				},

				totalAmount : reqBody.totalAmount
			});


			return user.save()
				.then(user => {
					console.log(`Order successful - User updated`);
					return true;
				})
				.catch(err => {
					console.log(err);
					return false;
				});
		}).catch(err => {
			console.log(err);
			console.log(`Error in retrieving the user`);
			return false;
			// return `Error in retrieving the user`;
		});


	let isProductUpdated = await Product.findById(reqBody.productId)
		.then(order => {

			order.userOrders.push({
				userId : userId
			});

			return order.save()
				.then(order => {
					console.log(`Order successful - Product updated`);
					return true;
				})
				.catch(err => {
					console.log(err);
					return false;
				});
		}).catch(err => {
			console.log(err);
			console.log(`Error in retrieving the Product`);
			return false;
			// return `Error in retrieving the Product`;
		});


	if (isUserUpdated && isProductUpdated) {
		console.log(`You have successfully ordered ${reqBody.productName}`);
		// return `You have successfully ordered ${reqBody.productName}`;
		// return `${isUserUpdated} and ${isProductUpdated}`;
		return true
	} else {
		console.log(`Your order of ${reqBody.productName} is unsuccessful`);
		// return `Your order of ${reqBody.productName} is unsuccessful`;
		//return `${isUserUpdated} and ${isProductUpdated}`;
		return false
	}
}




//retrieve user details

module.exports.getprofile = data => {
	return User.findById(data.userId)
		.then(result => {
			result.password = "****";
			return result;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
};




// set user as admin

module.exports.setAdmin = reqBody => {

	return User.findOne({ email : reqBody.email })
		.then(result => {

			if (result == null) {
				console.log(`Email is not registered`);
				return `Email is not registered`;
			} else if (result.isAdmin == true) {
				console.log(`Unable to update. User is already an admin.`);
				return `Unable to update. User is already an admin.`;
			} else {
				return User.findOneAndUpdate(
					{ email : reqBody.email },
					{ isAdmin : true }
					).then(updatedAdmin => {
						updatedAdmin.password = "****";
						return updatedAdmin;
					});
				
			};
		}).catch( err => {
			console.log(err);
			return `Error in updating Admin status`;
		});
};




// retrieve authenticated user's orders

module.exports.getMyOrders = data => {
	return User.findById(data.userId)
		.then(result => {
			return result.orderedProducts;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
};



// retrieve all orders (Admin only)

module.exports.getAllOrders = () => {
	return User.find()
		.then(result => {

			let list = result.map(order => {
				return order.orderedProducts;
			});

			return list;
/*			console.log(result);
			return result.orderedProducts;
			console.log(result.orderedProducts);*/
		})
		.catch(err => {
			console.log(err);
			return false;
		});
};



/*

module.exports.addToCart = async reqBody => {

	let cart ;

	// productId : reqBody.productId,
	// productName : reqBody.productName,
	// quantity : reqBody.quantity


	let addCart = await cart.findById(reqBody.productId)
		.then(result => {

			if (result == null) {
				cart.push({
					productId : reqBody.productId,
					productName : reqBody.productName,
					quantity : reqBody.quantity
				});

				return result.save()
					.then(result => )
			}

		})



}

*/