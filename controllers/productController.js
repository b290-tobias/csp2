const Product = require("../models/Product.js");
const User = require("../models/User.js");

// Product Creation

module.exports.createProduct = reqBody => {

	return Product.findOne({ name : reqBody.name }).then(result => {

		if (result == null) {

			let newProduct = new Product({
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price,
			});

			return newProduct.save().then(product => {
				return true})
					.catch(err => {
						console.log(err);
						console.log(`Error in adding a new product`);
						//return `Error in adding a new product`;
						return false;
					})
		} else {

			console.log(`Product name already exists. Please provide another product name.`);
			//return `Product name already exists. Please provide another product name.`;
			return false;
		}
	}).catch(err => {
		console.log(err);
		//return `Error in creating a product`;
		return false;
	});
};



// Get All Products (admin only)

module.exports.getAllProducts = () => {
	return Product.find({})
		.then(result => result)
		.catch(err => {
			console.log(err);
			// return `Error in getting All Products`;
			return false;
		});
};


// Get Active Products

module.exports.getActiveProducts = () => {
	return Product.find({ isActive : true })
		.then(result => result)
		.catch(err => {
			console.log(err);
			// return `Error in getting Active Products`;
			return false;
		});
}


// Get a specific product

module.exports.getProduct = reqParams => {
	return Product.findById(reqParams.productId)
		.then(result => result)
		.catch(err => {
			console.log(err);
			// return `Error in getting a Specific Product`;
			return false;
		});
};


// Update a specific product (admin only)

module.exports.updateProduct = (reqParams, reqBody) => {

	return Product.findOne({ name : reqBody.name }).then(result => {

		if (result == null || result.name == reqBody.name) {

			let updatedProduct = {
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price
			};

			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
				.then(result => {
					// return `Product is now updated.`;
					return true;
				})
				.catch(err => {
					console.log(err);
					// return `Error in updating the Product`;
					return false
				});

		} else {

			console.log(`Product name already exists. Please provide another product name.`);
			// return `Product name already exists. Please provide another product name.`;
			return false;
		}
	}).catch(err => {
		console.log(err);
		// return `Error in updating a product`;
		return false;
	});

};



// Archive a product (admin only)


module.exports.archiveProduct = (reqParams, reqBody) => {

	return Product.findById(reqParams.productId).then(result => {

		if (result == null) {

			console.log(`Product does not exists`);
			// return `Product does not exists`;
			return false;

		} else if (result.isActive == false) {

			console.log(`Product is already archived`);
			// return `Product is already archived`;
			return false;

		} else {

			let archivedProduct = { isActive : false };

			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
				.then(result => {
					// return `Product is now archived`;
					return true;
				})
				.catch(err => {
					console.log(err);
					// return `Error in archiving Product`
					return false;
				});
		}
	})
};


// Activate a product (admin only)


module.exports.activateProduct = (reqParams, reqBody) => {

	return Product.findById(reqParams.productId).then(result => {

		if (result == null) {

			console.log(`Product does not exists`);
			// return `Product does not exists`;
			return false;

		} else if (result.isActive == true) {

			console.log(`Product is already activated`);
			// return `Product is already activated`;
			return false;

		} else {

			let archivedProduct = { isActive : true };

			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
				.then(result => {
					// return `Product is now active.`;
					return true;
				})
				.catch(err => {
					console.log(err);
					// return `Error in archiving Product`
					return false;
				});
		}
	})
};





